<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Moja strona FB</title>
    <link href="../../static/css/bootstrap.css" rel="stylesheet">
    <link href="../../static/css/custom.css" rel="stylesheet">
    <link href="../../static/css/styles.css" rel="stylesheet">

</head>

<body>
<div id="wrap">
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://localhost:8080/">facebook.pl</a>
            </div>
            <div class="collapse navbar-collapse">
                <form class="navbar-form navbar-right" id="header-form" role="form">
                    <div class="lt-left">
                        <div class="form-group">
                            <label for="exampleInputEmail2">Email or Phone</label>
                            <input type="email" class="form-control input-sm" id="exampleInputEmail2"
                                   placeholder="Email or Phone">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword2">Password</label>
                            <input type="password" class="form-control input-sm" id="exampleInputPassword2"
                                   placeholder="Password">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                    <div class="lt-right">
                        <button type="submit" class="login-btn">Login</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="container" id="home">
        <div class="row">
            <div class="col-md-7">
                <h3 class="slogan">
                    Facebook helps you connect and share with the people in your life.
                </h3>
                <img src="../../static/img/background.png" class="img-responsive"/>
            </div>
            <div class="col-md-5">
                <form action="/thinview/main" method="POST" class="form" role="form">
                    <legend><a>Utworz konto</a></legend>
                    <h4>Jest to bezplatne i zawsze bedzie.</h4>
                    <div class="row">
                        <div class="col-xs-6 col-md-6" STYLE="padding-left: 0">
                            <input class="form-control input-lg" name="firstName" placeholder="First Name" type="text"
                                   autofocus/>
                        </div>
                        <div class="col-xs-6 col-md-6" style="padding-left: 0; padding-right: 0">
                            <input class="form-control input-lg" name="lastName" placeholder="Last Name" type="text"
                                   autofocus/>
                        </div>
                    </div>
                    <input class="form-control input-lg" name="login" placeholder="Your Email" type="email"/>
                    <input class="form-control input-lg" name="email" placeholder="Confirm Email" type="email"/>
                    <input class="form-control input-lg" name="passwordHash" placeholder="New Password"
                           type="password"/>
                    <label for="date">
                        Birth Date</label>
                    <div class="row">
                        <div class="col-xs-5 col-md-5" style="padding-left: 0">
                            <input type="date" name="dateOfBirth">
                        </div>

                        <div class="col-xs-7 col-md-7">
                            <label class="radio-inline">
                                <input type="radio" name="isFemale" id="inlineCheckbox1" value="false" checked/>
                                Male
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="isFemale" id="inlineCheckbox2" value="true"/>
                                Female
                            </label>
                        </div>
                        </div>

                    <span class="help-block">By clicking Create my account, you agree to our Terms and that you have read our Data Use Policy, including our Cookie Use.</span>
                        <button class="btn btn-lg btn-primary btn-block signup-btn" type="submit">
                            Create my account
                        </button>
                </form>

            </div>
        </div>
    </div>
</div>

<div id="footer">
    <div class="container">
        <p class="text-muted credit"><a href="http://facebook.pl">Facebook.com</a></p>
    </div>
</div>

<%--<script src="../../static/js/bootstrap/scripts.js"></script>
<script src="../../static/js/bootstrap/bootstrap.js"></script>--%>
</body>
</html>
