<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Facebook Main Page</title>
    <meta name="generator" content="Bootply"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="../../static/css/bootstrap.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="../../static/css/styles.css" rel="stylesheet">
</head>
<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">


            <!-- main right col -->
            <div class="column col-sm-12 col-xs-12" id="main">

                <!-- top nav -->
                <div class="navbar navbar-blue navbar-static-top">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="sr-only">Toggle</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="/" class="navbar-brand logo">b</a>
                    </div>
                    <nav class="collapse navbar-collapse" role="navigation">
                        <form class="navbar-form navbar-left">
                            <div class="input-group input-group-sm" style="max-width:360px;">
                                <input type="text" class="form-control" placeholder="Search" name="srch-term"
                                       id="srch-term">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i
                                            class="glyphicon glyphicon-search"></i></button>
                                </div>
                            </div>
                        </form>
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="#"><i class="glyphicon glyphicon-home"></i> Home</a>
                            </li>
                            <li>
                                <a href="#postModal" role="button" data-toggle="modal"><i
                                        class="glyphicon glyphicon-plus"></i> Post</a>
                            </li>
                            <li>
                                <a href="#"><span class="badge">badge</span></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                        class="glyphicon glyphicon-user"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="">More</a></li>
                                    <li><a href="">More</a></li>
                                    <li><a href="">More</a></li>
                                    <li><a href="">More</a></li>
                                    <li><a href="">More</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- /top nav -->

                <div class="padding">
                    <div class="full col-sm-9">

                        <!-- content -->
                        <div class="row">

                            <!-- main col left -->
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Profil</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3 col-lg-3 " align="center"><img alt="User Pic"
                                                                                                    src="../../static/img/1065-IMG_2529.jpg"
                                                                                                    class="img-circle img-responsive">
                                                </div>

                                                <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                                                  <dl>
                                                    <dt>DEPARTMENT:</dt>
                                                    <dd>Administrator</dd>
                                                    <dt>HIRE DATE</dt>
                                                    <dd>11/12/2013</dd>
                                                    <dt>DATE OF BIRTH</dt>
                                                       <dd>11/12/2013</dd>
                                                    <dt>GENDER</dt>
                                                    <dd>Male</dd>
                                                  </dl>
                                                </div>-->
                                                <div class=" col-md-9 col-lg-9 ">
                                                    <table class="table table-user-information">
                                                        <tbody>
                                                        <tr>
                                                            <td>Imie:</td>
                                                            <td>${firstName}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nazwisko:</td>
                                                            <td>${lastName}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Data urodzenia:</td>
                                                            <td>${dateOfBirth}</td>
                                                        </tr>

                                                        <tr>
                                                        <tr>
                                                            <td>Plec</td>
                                                            <td>${gender}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Na FB od:</td>
                                                            <td>${dateOfBirth}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td>${email}</td>
                                                        </tr>

                                                        </tbody>
                                                    </table>

                                                    <a href="#" class="btn btn-primary">Edytuj</a>
                                                    <a href="#" class="btn btn-primary">Usun</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <a data-original-title="Broadcast Message" data-toggle="tooltip"
                                               type="button" class="btn btn-sm btn-primary"><i
                                                    class="glyphicon glyphicon-envelope"></i></a>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- main col right -->
                            <div class="col-sm-6">

                                <div class="well">
                                    <form class="form-horizontal" role="form">
                                        <h4>What's New</h4>
                                        <div class="form-group" style="padding:14px;">
                                                    <textarea class="form-control"
                                                              placeholder="Update your status"></textarea>
                                        </div>
                                        <button class="btn btn-primary pull-right" type="button">Post</button>
                                        <ul class="list-inline">
                                            <li><a href=""><i class="glyphicon glyphicon-upload"></i></a></li>
                                            <li><a href=""><i class="glyphicon glyphicon-camera"></i></a></li>
                                            <li><a href=""><i class="glyphicon glyphicon-map-marker"></i></a>
                                            </li>
                                        </ul>
                                    </form>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading"><a href="#" class="pull-right">View all</a> <h4>
                                        Stackoverflow</h4></div>
                                    <div class="panel-body">
                                        <img src="//placehold.it/150x150" class="img-circle pull-right"> <a
                                            href="#">Keyword:
                                        Bootstrap</a>
                                        <div class="clearfix"></div>
                                        <hr>

                                        <p>If you're looking for help with Bootstrap code, the
                                            <code>twitter-bootstrap</code> tag at <a
                                                    href="http://stackoverflow.com/questions/tagged/twitter-bootstrap">Stackoverflow</a>
                                            is a good place to find answers.</p>

                                        <hr>
                                        <form>
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default">+1</button>
                                                    <button class="btn btn-default"><i
                                                            class="glyphicon glyphicon-share"></i></button>
                                                </div>
                                                <input type="text" class="form-control"
                                                       placeholder="Add a comment..">
                                            </div>
                                        </form>

                                    </div>
                                </div>


                            </div>
                        </div><!--/row-->

                        <div class="row">
                            <div class="col-sm-6">
                                <a href="#">Twitter</a>
                                <small class="text-muted">|</small>
                                <a href="#">Facebook</a>
                                <small class="text-muted">|</small>
                                <a href="#">Google+</a>
                            </div>
                        </div>

                        <div class="row" id="footer">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6">
                                <p>
                                    <a href="#" class="pull-right">Copyright 2018</a>
                                </p>
                            </div>
                        </div>

                        <hr>

                        <h4 class="text-center">
                            <a href="http://" target="ext">About KR</a>
                        </h4>

                        <hr>


                    </div><!-- /col-9 -->
                </div><!-- /padding -->
            </div>
            <!-- /main -->

        </div>
    </div>
</div>


<!--post modal-->
<div id="postModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                Update Status
            </div>
            <div class="modal-body">
                <form class="form center-block">
                    <div class="form-group">
                        <textarea class="form-control input-lg" autofocus=""
                                  placeholder="What do you want to share?"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div>
                    <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Post</button>
                    <ul class="pull-left list-inline">
                        <li><a href=""><i class="glyphicon glyphicon-upload"></i></a></li>
                        <li><a href=""><i class="glyphicon glyphicon-camera"></i></a></li>
                        <li><a href=""><i class="glyphicon glyphicon-map-marker"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- script references -->
<script src="../../static/js/bootstrap/jquery.js"></script>
<script src="../../static/js/bootstrap/bootstrap.js"></script>
<script src="../../static/js/bootstrap/scripts.js"></script>
</body>
</html>