package pl.facebook.services;

import com.sun.istack.internal.NotNull;
import pl.facebook.model.User;
import pl.facebook.model.UserData;

import java.util.List;
import java.util.Optional;

/**
 * Created by krogowski on 2017-12-16.
 */
public interface UserService {
    // user story - chce sprawdzić czy dany użytkownik (odanym loginie) już istnieje, więc
    // potrzebuje metody do sprawdzania tego:
    boolean userExists(String login);
    // user story - chcę zarejestrować użytkownika
    boolean registerUser(User userToRegister, UserData data);
    // user story - chcę wylistować wszystkich użytkowników
    List<User> getAllUsers();
    // user story - logowanie użytkownika
    boolean userLogin(String login, String passwordHash);

    void postSomething(String post, @NotNull User sender);

    Optional<User> getUserWithId(Long id);

    void addPost(Long idUser1, Long isUser2, String message);
}
