package pl.facebook.services;

import com.sun.istack.internal.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.facebook.dao.UserDao;
import pl.facebook.model.Post;
import pl.facebook.model.User;
import pl.facebook.model.UserData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public boolean userExists(String login) {
        return false;
    }

    @Override
    public boolean registerUser(User userToRegister, UserData dataToRegister) {
        userDao.registerUser(userToRegister, dataToRegister);
        return true;
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public boolean userLogin(String login, String passwordHash) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void postSomething(String post, @NotNull User sender) {

    }

    @Override
    public Optional<User> getUserWithId(Long id) {
        return userDao.findById(id);
    }

    @Override
    public void addPost(Long idUser1, Long idUser2, String message) {
        userDao.persistPosts(new Post(idUser1, idUser2, message));
    }
}