package pl.facebook.dao;

import com.sun.istack.internal.NotNull;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.facebook.model.Post;
import pl.facebook.model.User;
import pl.facebook.model.UserData;

import javax.transaction.Transaction;
import java.util.List;
import java.util.Optional;

@Repository(value = "userDao")
public class UserDaoImpl extends AbstractDao implements UserDao{

    @Override
    public void registerUser(User u, UserData data) {
        persist(data);
        u.setUserData(data);
        persist(u);
    }

    @Override
    public Optional<User> findById(Long id) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("id", id));

        return Optional.ofNullable((User) criteria.uniqueResult());
    }

    @Override
    public boolean userExist(@NotNull String withLogin) {
        return false;
    }

    @Override
    public List<User> getAllUsers() {
            return getSession().createCriteria(User.class).list();
    }

    @Override
    public void persistPosts(@NotNull Post post) {
        persist(post);

    }
}
