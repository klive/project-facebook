package pl.facebook.dao;

import com.sun.istack.internal.NotNull;
import pl.facebook.model.Post;
import pl.facebook.model.User;
import pl.facebook.model.UserData;

import javax.transaction.Transaction;
import java.util.List;
import java.util.Optional;

/**
 * Created by krogowski on 2017-12-16.
 */
public interface UserDao {
    void registerUser(User u, UserData data);

    Optional<User> findById(Long id);

    boolean userExist(@NotNull String withLogin);

    List<User> getAllUsers();

    public void persistPosts(@NotNull Post post);

}
