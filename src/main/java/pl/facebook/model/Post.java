package pl.facebook.model;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "posr_from")
    @JoinColumn(name = "user_id")
    private Long postFrom;

    @Column(name = "post_to")
    @JoinColumn(name = "user_id")
    private Long postedTo;

    private String postMessage;

    private Date createTime = Date.from(Instant.now());

    public Post(Long postFrom, Long postedTo, String postMessage) {
        this.postFrom = postFrom;
        this.postedTo = postedTo;
        this.postMessage = postMessage;
        this.createTime = createTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostFrom() {
        return postFrom;
    }

    public void setPostFrom(Long postFrom) {
        this.postFrom = postFrom;
    }

    public Long getPostedTo() {
        return postedTo;
    }

    public void setPostedTo(Long postedTo) {
        this.postedTo = postedTo;
    }

    public String getPostMessage() {
        return postMessage;
    }

    public void setPostMessage(String postMessage) {
        this.postMessage = postMessage;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", postFrom=" + postFrom +
                ", postedTo=" + postedTo +
                ", postMessage='" + postMessage + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
