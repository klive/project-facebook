package pl.facebook.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.internal.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "user_app")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    @Id
    @Column(name = "user_id")
    @GeneratedValue
    private long id;

    @Column
    @NotNull
    private String login;

    @Column
    @NotNull
    private String passwordHash;
    public User() {
        this.id = 0L;
    }
    public User(String login, String passwordHash) {
        this.id = 0L;
        this.login = login;
        this.passwordHash = passwordHash;
    }

    @OneToOne(cascade = CascadeType.PERSIST)
    private UserData data;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPasswordHash() {
        return passwordHash;
    }
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                '}';
    }

    public void setUserData(UserData data) {
        this.data = data;
    }
    public UserData getUserData() {
        return data;
    }
}
