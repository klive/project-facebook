package pl.facebook.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.facebook.model.User;
import pl.facebook.model.UserData;
import pl.facebook.model.responses.Response;
import pl.facebook.model.responses.ResponseFactory;
import pl.facebook.services.UserService;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest")
public class UserController {

    @Autowired
    UserService service;

    @RequestMapping(value = "/testUser", method = RequestMethod.GET)
    public ResponseEntity<User> requestTestUser() {
        return new ResponseEntity<User>(
                new User("maLogin", "maHaslo"), HttpStatus.OK);
    }

    @RequestMapping(value = "/listUsers", method = RequestMethod.GET)
    public ResponseEntity<List<User>> requestUserList() {
        return new ResponseEntity<List<User>>(service.getAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/registerUser", method = RequestMethod.GET)
    public ResponseEntity<String> registerUser(
            @RequestParam String userName,
            @RequestParam String password) {
        if (service.userExists(userName)) {
            return new ResponseEntity<String>(
                    "User with that login already exists!",
                    HttpStatus.BAD_REQUEST);
        } else {
            service.registerUser(new User(userName, password), new UserData("a", "b", "c", false, Date.from(Instant.now()), Date.from(Instant.now())));
            return new ResponseEntity<String>("OK", HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> getUserWithId(@PathVariable Long id) {
        Optional<User> user = service.getUserWithId(id);
        if (user.isPresent()) {
            return new ResponseEntity<Response>(ResponseFactory.success(user.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with that id does not exist."), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/addPost/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> executePost(@PathVariable Long id,
                                                @RequestParam Long idUser2,
                                                @RequestParam String message) {
        service.addPost(id, idUser2, message);
        return new ResponseEntity<Response>(ResponseFactory.success(), HttpStatus.OK);
    }
}
