package pl.facebook.controllers;

import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.facebook.model.User;
import pl.facebook.model.UserData;
import pl.facebook.services.UserService;

import java.util.Optional;

@Controller
@RequestMapping(value = "/thinview")
public class IndexControllerThin {

    @Autowired
    UserService service;

    @RequestMapping(value = "/userProfile/{id}")
    public String getUserProfile(ModelMap model, @PathVariable Long id) {
        model.addAttribute("userid", id);
        Optional<User> user = service.getUserWithId(id);
        System.out.println(user);
        if (user.isPresent()) {
            User u = user.get();
            model.addAttribute("login", u.getLogin());
            model.addAttribute("passwordHash", u.getPasswordHash());
            model.addAttribute("firstName", u.getUserData().getFirstName());
            model.addAttribute("lastName", u.getUserData().getLastName());
            model.addAttribute("dateOfBirth", u.getUserData().getDateOfBirth());
            model.addAttribute("email", u.getUserData().getEmail());
            model.addAttribute("dateOfBirth", u.getUserData().getDateOfBirth());
            model.addAttribute("gender", u.getUserData().isFemale());
        }
        return "UserProfileThin";
    }

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String userRegister(@ModelAttribute("form") @Validated User user, BindingResult br) {
        if (br.hasErrors()) {
            return "Main";
        } else {
            return "redirect:/";
        }
    }

    @GetMapping("/")
    public String showForm(Model model) {
        return "LoginAndRegister";
    }

    @RequestMapping(value = "/main", method = RequestMethod.POST)
    public String register(@Validated User user, @Validated UserData data, @RequestParam String isFemale) {
        System.out.println(user);
        System.out.println(user.getUserData());
        if (isFemale.equals("true")) {
            data.setFemale(true);
        } else {
            data.setFemale(false);
        }
        service.registerUser(user, data);
        return "redirect:/thinview/";
    }
}
