'use strict';

angular.module('faceapp').factory('UserService', ['$http', '$q',
    function ($http, $q) {

        var RESTAPIURL = 'http://localhost:8080/rest/';

        var factory = {
            fetchAllUsers: fetchAllUsers
        };
        console.log('Factory created.');
        return factory;

        function fetchAllUsers() {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'listUsers').then(
                function (data) {
                    // sukces
                    deferred.resolve(data);
                }, function (result) {
                    // nie udalo sie
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

    }]);