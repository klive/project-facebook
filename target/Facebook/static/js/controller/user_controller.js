'use strict';
angular.module('coinapp').controller('UserController', ['$scope', 'UserService', function ($scope, UserService) {
    var self = this;
    self.users = [];
    self.user = {};
    console.log('Controller created.');
    self.fetchAllUsersMethod = fetchAllUsers;
    self.fetchUserWithIdMethod = fetchUserWithId;
    fetchAllUsers();
    function fetchAllUsers() {
        UserService.fetchAllUsers().then(function (response) {
                console.log(response);
                self.users = response.data;
            }, // ^ resolve
            function (result) {
                console.log(result);
            } // ^ reject
        );
    }

// Tu jestem w kontrolerze
    function fetchUserWithId(userId) {
        console.log('Fetch user with id:' + userId);
        UserService.fetchUserWithId(userId).then(function (response) {
                console.log(response);
                self.user = response.data.result.resultObject;
            }, // ^ resolve
            function (result) {
                console.log(result);
            } // ^ reject
        );
    }

    $scope.onloadFunction = function (id) {
        console.log('fuwd:' + id);
        fetchUserWithId(id);
    }
}]);